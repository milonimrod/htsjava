import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileHeader.SortOrder;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CleanTags {
	public static void main(String[] args) throws IOException, ParseException {
		Options options = new Options();
		options.addOption("h", "help", false, "print this help");
		options.addOption("v", "verbose", false,
				"Print information to the screen");
		CommandLineParser parser = new GnuParser();
		CommandLine cmdLine = parser.parse(options, args, true);

		if (cmdLine.hasOption('h')) {
			printHelp(options);
			System.exit(0);
		}

		String[] remaining = cmdLine.getArgs();
		if (remaining.length != 2) {
			System.err.println("ERROR: please provide correct input !!!");
			printHelp(options);
			System.exit(1);
		}

		final boolean verbose = cmdLine.hasOption('v');

		if (verbose) {
			System.out.println("Using arguments " + Arrays.toString(args));
		}

		String inputFile = remaining[0];
		String[] tags = remaining[1].split(",");
		for (int i = 0; i < tags.length; i++) {
			if (tags[i].length() != 2) {
				System.out.println("SAM tags must be 2 letters");
			}
		}

		final SamReader reader = SamReaderFactory.makeDefault().open(
				new File(inputFile));
		// open all the output writers
		SAMFileWriter outputSamPassed = null;

		// get the input file name
		String outString = "";
		if (inputFile.toLowerCase().endsWith(".bam")) {
			outString = inputFile.substring(0, inputFile.lastIndexOf(".bam"));
		} else if (inputFile.toLowerCase().endsWith(".sam")) {
			outString = inputFile.substring(0, inputFile.lastIndexOf(".sam"));
		}
		if (verbose) {
			System.out.println(String.format(
					"Using %s as output file prefix\n", outString));

			System.out.println(reader.getFileHeader());
		}
		SAMFileHeader writeHeader = new SAMFileHeader();
		writeHeader.setSortOrder(SortOrder.unsorted);
		writeHeader.setProgramRecords(reader.getFileHeader()
				.getProgramRecords());
		writeHeader.setSequenceDictionary(reader.getFileHeader()
				.getSequenceDictionary());
		final int BUFFER_SIZE = 512 * 1024 * 1024;
		outputSamPassed = new SAMFileWriterFactory()
				.setCreateIndex(true)
				.setUseAsyncIo(true)
				.setBufferSize(BUFFER_SIZE)
				.makeBAMWriter(
						writeHeader,
						true,
						new BufferedOutputStream(new FileOutputStream(new File(
								outString + "_clean.bam")), BUFFER_SIZE));

		BufferedWriter logWriter = new BufferedWriter(new FileWriter(new File(
				outString + "-cleanTag.log")));

		logWriter.write("arguments: " + Arrays.toString(args) + "\n");

		long globalTime = System.currentTimeMillis();
		long count = 0;
		for (final SAMRecord samRecord : reader) {
			count++;
			if (verbose && count % 1000000 == 0) {
				// read per second
				long time = System.currentTimeMillis() - globalTime;
				int numPerSeconds = (int) (1000 * count / time);
				System.out.println(String.format(
						"Computed %d reads in a rate of %d reads / sec.",
						count, numPerSeconds));
			}
			// System.out.println(samRecord.getAttribute("NH"));
			for (int i = 0; i < tags.length; i++) {
				samRecord.setAttribute(tags[i], null);
			}
			outputSamPassed.addAlignment(samRecord);
		}

		reader.close();
		outputSamPassed.close();
		logWriter.write("Time: " + (System.currentTimeMillis() - globalTime)
				+ "\n");
		logWriter.close();
	}

	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.setWidth(200);
		formatter.printHelp(FilterByRegion.class.getName()
				+ " [Optionts] <input bam> tag1[,tag2,tag3]", options);
	}
}
