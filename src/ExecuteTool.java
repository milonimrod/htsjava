import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

import org.apache.commons.cli.ParseException;


public class ExecuteTool {
	public static void main(String[] args) throws IOException, ParseException {
		HashSet<String> set = new HashSet<String>();
		set.add("filter");
		set.add("cleanTag");
		
		if (args.length == 0 || !set.contains(args[0])){
			System.out.println("Command not recognized.");
			System.out.println("Usage: HTSjava <command> <arguments>\n");
			System.out.println("filter\tFilter BAM/SAM records that 'stab' given regions.");
			System.out.println("cleanTag\tClear BAM/SAM records from given tags.");
			System.exit(1);
		}
		
		String command = args[0];
		args = Arrays.copyOfRange(args, 1, args.length);
		switch (command) {
		case "filter":
			FilterByRegion.main(args);
			break;
		case "cleanTag":
			CleanTags.main(args);
			break;
		default:
			break;
		}
	}
}
