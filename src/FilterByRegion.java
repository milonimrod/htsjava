import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import htsjdk.samtools.SAMFileHeader;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMSequenceDictionary;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class FilterByRegion {
	public static void main(String[] args) throws IOException, ParseException {
		Options options = new Options();
		options.addOption("c", "count", false, "Only print the counters.");
		options.addOption("h", "help", false, "print this help");
		options.addOption("v", "verbose", false,
				"Print information to the screen");
		CommandLineParser parser = new GnuParser();
		CommandLine cmdLine = parser.parse(options, args, true);

		if (cmdLine.hasOption('h')) {
			printHelp(options);
			System.exit(0);
		}

		String[] remaining = cmdLine.getArgs();
		if (remaining.length != 2) {
			System.err.println("ERROR: please provide correct input !!!");
			printHelp(options);
			System.exit(1);
		}

		final boolean produceFiles = !cmdLine.hasOption('c');
		final boolean verbose = cmdLine.hasOption('v');

		if (verbose) {
			System.out.println("Using arguments " + Arrays.toString(args));
		}

		String inputFile = remaining[0];
		String maskingFile = remaining[1];

		final SamReader reader = SamReaderFactory.makeDefault().open(
				new File(inputFile));
		// open all the output writers
		SAMFileWriter outputSamPassed = null;
		SAMFileWriter outputSamFailed = null;

		// get the input file name
		String outString = "";
		if (inputFile.toLowerCase().endsWith(".bam")) {
			outString = inputFile.substring(0, inputFile.lastIndexOf(".bam"));
		} else if (inputFile.toLowerCase().endsWith(".sam")) {
			outString = inputFile.substring(0, inputFile.lastIndexOf(".sam"));
		}
		if (verbose) {
			System.out.println(String.format(
					"Using %s as output file prefix\n", outString));
		}

		if (produceFiles) {
			SAMFileHeader writeHeader = new SAMFileHeader();
			writeHeader.setSortOrder(reader.getFileHeader().getSortOrder());
			writeHeader.setProgramRecords(reader.getFileHeader()
					.getProgramRecords());
			writeHeader.setSequenceDictionary(reader.getFileHeader()
					.getSequenceDictionary());
			final int BUFFER_SIZE = 512 * 1024 * 1024;
			outputSamPassed = new SAMFileWriterFactory()
					.setCreateIndex(true)
					.setUseAsyncIo(true)
					.setBufferSize(BUFFER_SIZE)
					.makeBAMWriter(
							writeHeader,
							true,
							new BufferedOutputStream(new FileOutputStream(
									new File(outString + "_passed.bam")),
									BUFFER_SIZE));

			outputSamFailed = new SAMFileWriterFactory()
					.setCreateIndex(true)
					.setUseAsyncIo(true)
					.setBufferSize(BUFFER_SIZE)
					.makeBAMWriter(
							writeHeader,
							true,
							new BufferedOutputStream(new FileOutputStream(
									new File(outString + "_failed.bam")),
									BUFFER_SIZE));
		}
		BufferedWriter logWriter = new BufferedWriter(new FileWriter(new File(
				outString + ".log")));
		logWriter.write("### Arguments ###\n");
		logWriter.write("arguments: " + Arrays.toString(args) + "\n");

		// Counter for features
		TObjectIntHashMap<String> counterPerFeature = new TObjectIntHashMap<String>();
		HashSet<String> presentFeatures = new HashSet<String>();
		// read GTF file
		ArrayList<PositionInfo> list = readGTF(reader.getFileHeader()
				.getSequenceDictionary(), maskingFile, presentFeatures);
		TIntObjectHashMap<ArrayList<PositionInfo>> referencesMap = new TIntObjectHashMap<ArrayList<PositionInfo>>();
		for (PositionInfo pos : list) {
			if (!referencesMap.containsKey(pos.reference)) {
				referencesMap.put(pos.reference, new ArrayList<PositionInfo>());
			}
			referencesMap.get(pos.reference).add(pos);
		}

		if (verbose) {
			System.out.println("Got the following features "
					+ presentFeatures.toString());
		}

		long globalTime = System.currentTimeMillis();
		long passedAmount = 0, failedAmount = 0;
		long count = 0;
		boolean failed = false;
		for (final SAMRecord samRecord : reader) {
			count++;
			if (verbose && count % 1000000 == 0) {
				// read per second
				final long time = System.currentTimeMillis() - globalTime;
				final int numPerSeconds = (int) (1000 * count / time);
				System.out.println(String.format(
						"Computed %d reads in a rate of %d reads / sec.",
						count, numPerSeconds));
			}
			failed = false;
			// Only pass on the list if the current record reference index is in
			// found at the masking file
			if (referencesMap.containsKey(samRecord.getReferenceIndex())) {
				list = referencesMap.get(samRecord.getReferenceIndex());
				for (int i = 0; i < list.size() && !failed; i++) {
					if (list.get(i).overlap(samRecord, false) > 0) {
						failed = true;
						counterPerFeature.adjustOrPutValue(list.get(i).feature,
								1, 1);
					}
				}
			}
			if (failed) {
				if (produceFiles) {
					outputSamFailed.addAlignment(samRecord);
				}
				failedAmount++;
			} else {
				if (produceFiles) {
					outputSamPassed.addAlignment(samRecord);
				}
				passedAmount++;
			}

		}

		reader.close();
		if (produceFiles) {
			outputSamPassed.close();
			outputSamFailed.close();
		}
		// print statistics
		final String stats = String.format(
				"total: %d%npassed: %d%nfailed: %d%npercentage failed: %f%n",
				passedAmount + failedAmount, passedAmount, failedAmount,
				(100.0 * failedAmount / (failedAmount + passedAmount)));
		System.out.println(stats);
		logWriter.write("### Counters ###\n");
		logWriter.write(stats);

		// Print per feature amounts
		logWriter.write("### Per Feature Counters ###\n");
		for (String key : counterPerFeature.keySet()) {
			logWriter.write(key + ":" + counterPerFeature.get(key) + "\n");
		}
		logWriter.write("### Computation time ###\n");
		logWriter.write("Time: " + (System.currentTimeMillis() - globalTime)
				+ "\n");
		logWriter.close();
	}

	private static ArrayList<PositionInfo> readGTF(
			SAMSequenceDictionary samSequenceDictionary, String string,
			HashSet<String> presentFeatures) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(new File(
				string)));
		ArrayList<PositionInfo> list = new ArrayList<PositionInfo>();
		String line = reader.readLine();
		while (line != null) {
			line = line.trim();
			if (line.length() > 0) {
				String[] sp = line.split("\t");
				String geneID = sp[8].split(";")[0];
				geneID = geneID.substring(geneID.indexOf(" \"") + 2,
						geneID.length() - 1);

				PositionInfo p = new PositionInfo(
						samSequenceDictionary.getSequenceIndex(sp[0]),
						Integer.parseInt(sp[3]), Integer.parseInt(sp[4]),
						geneID);
				presentFeatures.add(geneID);
				list.add(p);
			}
			line = reader.readLine();
		}
		reader.close();
		return list;
	}

	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.setWidth(200);
		formatter.printHelp(FilterByRegion.class.getName()
				+ " [Optionts] <input bam> <masking gtf>", options);
	}
}

class PositionInfo {
	int reference;
	int start;
	int end;
	String feature;

	public PositionInfo(int refName, int start, int end, String feature) {
		this.reference = refName;
		this.start = start;
		this.end = end;
		this.feature = feature;
	}

	public int overlap(SAMRecord record, boolean checkRef) {

		if (checkRef && this.reference != record.getReferenceIndex()) {
			return 0;
		}

		return this.intersection(record.getAlignmentStart(),
				record.getAlignmentEnd());
	}

	private int intersection(int b1, int b2) {
		final int l0 = Math.max(start, b1);
		final int l1 = Math.min(end, b2);
		final int diff = l1 - l0 + 1;

		return diff;
	}
}